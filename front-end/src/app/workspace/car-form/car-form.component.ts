import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Voiture } from 'src/app/models/voiture.model';
import { ClientService } from 'src/app/services/client.service';
import { NotificationHandlerService } from 'src/app/services/notification.handler.service';

@Component({
  selector: 'app-car-form',
  templateUrl: './car-form.component.html',
  styleUrls: ['./car-form.component.css']
})
export class CarFormComponent {
  voitureForm : FormGroup = this.formBuilder.group({
    marque : ['', Validators.required],
    matricule : ['', [Validators.required, Validators.pattern("[0-9]{4}[a-zA-Z]{3}")]]
  });

  @Input() btnCloseModal : any = undefined;
  isLoading : boolean = false;
  response : any;

  constructor(
    private clientService : ClientService,
    private notificationHandler : NotificationHandlerService,
    private formBuilder : FormBuilder
  ){}

  clean(){
    this.isLoading = false;
    this.response = undefined;
    this.voitureForm.reset();
  }

  onSubmitForm(){
    this.isLoading = true;
    const formValue = this.voitureForm.value;
    let voiture = new Voiture(null, formValue['marque'], formValue['matricule']);
    let observale = this.clientService.addVoiture(voiture);
    let subscription = observale.subscribe({
        next : (response) => {
            this.response = response;
            this.isLoading = false;
            if(response.metaData.status == 200){
              if(this.btnCloseModal) this.btnCloseModal.click();
              this.clean();
              this.notificationHandler.raiseSuccess({message : "Voiture ajouter"});
            }
        },
        error : (error) => {
          if(error.error.metaData){
            this.response = error.error;
          }
          else this.notificationHandler.raireError(error);
          this.isLoading = false;
        },
        complete : () => {
          subscription.unsubscribe();
        }
    })
  }
}
