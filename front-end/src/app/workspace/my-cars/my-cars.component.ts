import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Voiture } from 'src/app/models/voiture.model';
import { ClientService } from 'src/app/services/client.service';
import { NotificationHandlerService } from 'src/app/services/notification.handler.service';

@Component({
  selector: 'app-my-cars',
  templateUrl: './my-cars.component.html',
  styleUrls: ['./my-cars.component.css']
})
export class MyCarsComponent implements OnInit, OnDestroy {
  listeVoiture : Voiture[] = []

  voituresSubscription : Subscription | undefined = undefined;

  isLoading : boolean = false;

  constructor(
    private clientService : ClientService,
    private notificationHandler : NotificationHandlerService,
  ){}

  ngOnInit(): void {
    this.voituresSubscription = this.clientService.voituresSubject.subscribe({
      next : (voitures)=>{
        this.listeVoiture = voitures;
      }
    });
    this.clientService.emitVoituresSubject();
  }

  ngOnDestroy(): void {
    this.voituresSubscription?.unsubscribe();
  }

  edit(id : string | undefined | null){
    console.log(id);
  }

  remove(id : string | undefined | null){
    this.isLoading = true;
    let observale = this.clientService.removeVoiture(id);
    let subscription = observale.subscribe({
        next : (response : any) => {
            if(response.metaData.status == 200){
              this.notificationHandler.raiseSuccess({message : "Voiture supprimée"});
            }
            else{
              this.notificationHandler.raireError(response);
            }
        },
        error : (error : any) => {
          this.notificationHandler.raireError(error);
        },
        complete : () => {
          this.isLoading = false;
          subscription.unsubscribe();
        }
    })
  }

  onDrop(event: CdkDragDrop<string[]>) {
    this.clientService.deplacerVoiture(event.previousIndex, event.currentIndex)
  }
}
