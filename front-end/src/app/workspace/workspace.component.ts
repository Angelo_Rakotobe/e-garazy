import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Utilisateur } from '../models/utilisateur.model';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.css']
})
export class WorkspaceComponent implements OnInit {

  utilisateur : Utilisateur | undefined = undefined;

  constructor(
    private authService: AuthService,
    private router : Router
  ) { 
    this.utilisateur = this.authService.getUtilisateur()
  }
  ngOnInit(): void {
    
  }

  logout() {
    this.authService.removeToken();
    this.router.navigate(['login'])
  }

  isSideBarHide : boolean = false;

  toggleSidebar(){
    this.isSideBarHide = !this.isSideBarHide;
  }

  isSearchBarHide : boolean = false;

  toggleSearchBar(){
    this.isSearchBarHide = !this.isSearchBarHide;
  }
}
