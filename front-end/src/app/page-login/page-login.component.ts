import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Utilisateur } from '../models/utilisateur.model';
import { AuthService } from '../services/auth.service';
import { NotificationHandlerService } from '../services/notification.handler.service';

@Component({
  selector: 'app-page-login',
  templateUrl: './page-login.component.html',
  styleUrls: ['./page-login.component.css']
})
export class PageLoginComponent implements OnInit{
  utilisateurForm : FormGroup = this.formBuilder.group({
    email : 'sombiniaina@mean.com',
    motDePasse : '123456'
  });

  response : any;

  isLoading : boolean = false;

  constructor(
    private notificationHandler : NotificationHandlerService,
    private formBuilder : FormBuilder,
    private authService : AuthService,
    private router : Router
  ){}

  ngOnInit(): void {}

  onSubmitForm(){
    this.isLoading = true;
    const formValue = this.utilisateurForm.value;
    let utilisateur = new Utilisateur(formValue['nom'],formValue['prenom'],formValue['email'], formValue['motDePasse']);
    let observale = this.authService.auth(utilisateur);
    let subscription = observale.subscribe({
        next : (response) => {
          setTimeout( () => {
            this.response = response;
            this.isLoading = false;
            if(response.metaData.status == 200){
              this.authService.setData(response.data);
              this.router.navigate(["workspace"]);
            }
          }, 3000)
        },
        error : (error) => {
          if(error.error.metaData){
            this.response = error.error;
          }
          else this.notificationHandler.raireError(error);
          this.isLoading = false;
        },
        complete : () => {
          subscription.unsubscribe();
        }
    })
  }

  goToRegisterPage(e : Event){
    e.preventDefault();
    this.router.navigate(["register"]);
  }
}
