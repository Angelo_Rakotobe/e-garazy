import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { Utilisateur } from '../models/utilisateur.model';
import { environment } from './../../environments/environment';
import { NotificationHandlerService, USER_ALREADY_LOGGED, USER_NOT_LOGGED } from './notification.handler.service';

const TOKEN_VALUE : string = "toke-value";
const TOKEN_EXPIRARTION_DATE : string = "token-expiration-date";
const NOM : string = "nom";
const PRENOM : string = "prenom";
const EMAIL : string = "email";
const PSEUDO : string = "pseudo";
const ROLE : string = "role";

@Injectable()
export class AuthService implements CanActivate{


    constructor(
        private httpclient : HttpClient,
        private router : Router,
        private notificationHandler : NotificationHandlerService
    ){}

    auth(utilisateur: Utilisateur) : Observable<any>{
        return this.httpclient.post(environment.apiUrl + "/api/utilisateur/connexion", utilisateur);
    }

    signUp(utilisateur: Utilisateur) : Observable<any>{
        return this.httpclient.post(environment.apiUrl + "/api/utilisateur/inscription", utilisateur);
    }

    setData(data : any){
        localStorage.clear();
        localStorage.setItem(EMAIL, data.email);
        localStorage.setItem(ROLE, data.role);
        localStorage.setItem(TOKEN_VALUE, data.token.value);
        localStorage.setItem(TOKEN_EXPIRARTION_DATE, data.token.expiration);
    }

    isAuth(){
        return (localStorage.getItem(TOKEN_VALUE) != null && localStorage.getItem(TOKEN_EXPIRARTION_DATE) != null);
    }

    getToken(){
        return localStorage.getItem(TOKEN_VALUE);
    }

    getUtilisateur() : Utilisateur{
        const utilisateur = new Utilisateur(localStorage.getItem(NOM),localStorage.getItem(PRENOM),localStorage.getItem(PSEUDO),localStorage.getItem(EMAIL), localStorage.getItem(ROLE));
        return utilisateur;
    }

    removeToken(){
        localStorage.clear();
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        // quand l'utilisateur entre dans "/"
        if(route.url.length == 0){
            if(this.isAuth()) this.goToWorkspace(false);
            else this.goToLogin(false);
        }
        else{
            // Quand l'utilisateur veut consulter la page d'auth ou d'inscription alors qu'il est déja connecté.
            if((route.url[0].path == 'login' || route.url[0].path == 'register') && this.isAuth() ){
                this.goToWorkspace(true);
            }
            // Quand l'utilisateur veut consulter autre page alors qu'il n'est pas connecté.
            else if((route.url[0].path != 'login' && route.url[0].path != 'register') && !this.isAuth()){
                this.goToLogin(true);
            }
        }
        return true;
    }

    private goToLogin(raiseError : boolean){
        if(raiseError) this.notificationHandler.raireError({message : USER_NOT_LOGGED});
        this.router.navigate(['/login']);
    }

    private goToWorkspace(raiseError : boolean){
        if(raiseError) this.notificationHandler.raireError({message : USER_ALREADY_LOGGED});
        this.router.navigate(['/workspace']);
    }
}
