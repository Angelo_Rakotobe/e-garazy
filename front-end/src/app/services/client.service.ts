import { moveItemInArray } from "@angular/cdk/drag-drop";
import { Observable, Subject } from "rxjs";
import { Voiture } from "../models/voiture.model";

export class ClientService {
	private voitures : Voiture[] = [
		new Voiture("1" ,"GOLF V", "4556TBF"),
		new Voiture("2", "AUDI TT", "2526TAY"),
		new Voiture("3", "NISSAN PATROL", "4589TBN")
	];

	// subject 
	voituresSubject : Subject<Voiture[]> = new Subject<Voiture[]>();

	constructor() { }

	// auth(utilisateur: Utilisateur): Observable<any> {
	// 	return this.httpclient.post(environment.apiUrl + "/api/utilisateur/connexion", utilisateur);
	// }

	emitVoituresSubject(){
		this.voituresSubject.next(this.voitures.slice()); 
	}

	deplacerVoiture(previousIndex : number, currentIndex : number){
		moveItemInArray(this.voitures, previousIndex, currentIndex);
		this.emitVoituresSubject();
	}

	addVoiture(voiture : Voiture) : Observable<any>{
		const observale = new Observable((subscriber) => {
			setTimeout(()=>{
				voiture.setId(String(this.makeid(10)));
				voiture.setNew(true);
				subscriber.next(
					{
						metaData : {
							status : 200,
							message : "OK",
							errors : null
						},
						data : []
					}
				);
				subscriber.complete();
			}, 3000)
		})

		const subscription = observale.subscribe({
			next : (response : any) => {
				if(response.metaData.status == 200) {
					this.voitures.unshift(voiture);
					this.emitVoituresSubject();	
				}
			},
			complete : () => {
				subscription.unsubscribe();
			}
		})
		return observale;
	}

	removeVoiture(id : string | undefined | null) : Observable<any>{
		const observale = new Observable((subscriber) => {
			setTimeout(() => {
				subscriber.next(
					{
						metaData : {
							status : 200,
							message : "OK",
							errors : null
						},
						data : []
					}
				);
				subscriber.complete();
			}, 3000)
		})

		const subscription = observale.subscribe({
			next : (response : any) => {
				if(response.metaData.status == 200) {
					this.voitures = this.voitures.filter(function( voiture ) {
						return voiture.getId() !== id;
					});
					this.emitVoituresSubject();	
				}
			},
			complete : () => {
				subscription.unsubscribe();
			}
		})
		return observale;
	}

	// Fonction temporaire akaa idFotsiny
	makeid(length : number) {
		var result           = '';
		var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()-_=+{}[]:;<>,./?';
		var charactersLength = characters.length;
		for ( var i = 0; i < length; i++ ) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		return result;
	}
}
