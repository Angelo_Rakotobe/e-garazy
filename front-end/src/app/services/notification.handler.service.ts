import { Subject } from "rxjs";


export const USER_ALREADY_LOGGED = "T'es con ou quoi? tu es déjà connecté FDP"
export const USER_NOT_LOGGED = "Non, cela ne se fait pas, connecte-toi d'abord FDP."
export const CONNECTON_LOST = "Vous etes hors connexion"


export class NotificationHandlerService {
    errorSubject = new Subject<any>();
    successSubject = new Subject<any>();

    constructor(){}

    raireError(error : any){
        this.errorSubject.next(error.message);
    }

    raiseSuccess(success : any){
        this.successSubject.next(success.message);
    }
}