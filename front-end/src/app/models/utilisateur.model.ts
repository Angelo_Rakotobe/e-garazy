export class Utilisateur{
    private nom: string | undefined | null;
    private prenom: string | undefined | null;
    private pseudo: string | undefined | null;
    private email: string | undefined | null;
    private motDePasse: string | undefined | null;

    /*constructor(email: string | undefined | null, motDePasse: string | undefined | null){
        this.email = email;
        this.motDePasse = motDePasse;
    }
*/
   constructor(nom: string | undefined | null,prenom: string | undefined | null,email: string | undefined | null, motDePasse: string | undefined | null,pseudo?: string | undefined | null){
            this.nom = nom;
            this.prenom = prenom;
            this.email = email;
            this.motDePasse = motDePasse;
            if(pseudo) this.pseudo = pseudo;
        }

    getEmail() : string | undefined | null{
        return this.email;
    }
}
