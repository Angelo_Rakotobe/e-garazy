export class Voiture{
    private id: string | undefined | null;
    private marque: string | undefined | null;
    private matricule: string | undefined | null;
    private new : boolean | undefined;

    constructor(id : string | undefined | null, marque: string | undefined | null, matricule: string | undefined | null){
        this.id = id;
        this.marque = marque?.toUpperCase();
        this.matricule = matricule?.toUpperCase();   
        this.new = false; 
    }

    setId(id : string | undefined | null){
        this.id = id;
    }

    getId() : string | undefined | null{
        return this.id;
    }

    getMarque() : string | undefined | null{
        return this.marque;
    }

    getMatricule() : string | undefined | null{
        return this.matricule;
    }

    setNew(val : boolean | undefined){
        this.new = val;
    }

    isNew(){
        return this.new
    }
}