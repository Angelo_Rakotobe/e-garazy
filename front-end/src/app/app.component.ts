import { Component, isDevMode, OnDestroy, OnInit } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { NotificationHandlerService, CONNECTON_LOST } from './services/notification.handler.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy{
  errorMessage : any;
  successMessage : any;
  subscriptions : any[] = [];

  constructor(
    private notificationHandler : NotificationHandlerService
  ){

  }

  private removeError(){
    setTimeout( () => {
      this.errorMessage = undefined;
    }, 3000)
  }

  private removeSuccess(){
    setTimeout( () => {
      this.successMessage = undefined;
    }, 3000)
  }
  
  ngOnInit(): void {
    // forErrors
    this.subscriptions.push(this.notificationHandler.errorSubject.subscribe({
      next : (message) => {
        this.errorMessage = message;
        this.removeError()
      }
    }))

    // For success
    this.subscriptions.push(this.notificationHandler.successSubject.subscribe({
      next : (message) => {
        this.successMessage = message;
        this.removeSuccess()
      }
    }))

    this.subscriptions.push(fromEvent(window, 'offline').subscribe({
      next : () => {
        this.errorMessage = CONNECTON_LOST;
        this.removeError()
      }
    }))
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
