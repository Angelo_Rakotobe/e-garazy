const express = require('express');
const router = express.Router();
const utilisateurRequestHandler = require('../RequestHandlers/UtilisateurRequestHandler');

router.use(express.json()); //middleware json integre
router.post('/inscription', utilisateurRequestHandler.inscription);
router.post('/connexion', utilisateurRequestHandler.connexion)

module.exports = router;