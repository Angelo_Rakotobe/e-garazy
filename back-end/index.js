require('dotenv').config()
const express = require('express');
const cors = require('cors');
const utilisateurRoutes = require('./Routes/UtilisateurRoutes');
let app = express();

// const querystring = require('querystring');
// const url = require('url');
// const bodyParser = require('body-parser');
// const Utilisateur = require('./Models/Utilsateur');

// app.use(bodyParser.json()) // Ajoute les parametres de body raw json dans le champ "body"  de l'objet request.
// app.use(function(request, response, next){ // Ajoute les parametres d'url dans le champ "urlParams"  de l'objet request.
//     request.urlParams =  querystring.parse(url.parse(request.url).query)
//     next();
// })

app.use(cors()); // CORS
// Routes
app.use('/api/utilisateur', utilisateurRoutes);


app.listen(process.env.PORT, function(){
    console.log("Serveur demaré");
})