
class HttpResponse {
    static mapCodeMessage = {
        200 : "OK",
        400 : "BAD_REQUEST",
        401 : "UNAUTHORISED",
        403 : "FORBIDDEN",
        404 : "NOT_FOUND",
        500 : "INTERNAL_SERVER_ERROR",
        501 : "NOT_IMPLEMENTED",
        503 : "SERVICE_UNVAILABLE",
    }

    constructor(){
        this.metaData = {};
        this.metaData.status = 200;
        this.metaData.message = HttpResponse.mapCodeMessage[200];
        this.metaData.errors = null;
        this.data = null;
    }

    setStatus(statusCode){
        this.metaData.status = statusCode;
        this.metaData.message = HttpResponse.mapCodeMessage[statusCode];
    }

    getStatus(){
        return this.metaData.status
    }

    setErrors(errors){
        this.metaData.errors = errors;
    }

    setData(data){
        this.data = data;
    }

}

module.exports = HttpResponse;