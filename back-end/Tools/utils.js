const crypto = require('crypto')

function token() {
    const shasum = crypto.createHash('sha1')
    shasum.update(new Date().toString());
    return shasum.digest('hex');
};

exports.token = token