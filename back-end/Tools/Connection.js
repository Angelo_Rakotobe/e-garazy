const connect = async (success) => {
    const mongoose = require('mongoose');
    mongoose.set('strictQuery', false)
    //connection
    await mongoose.connect(process.env.DATABASE_CONNECTION_STRING)

    // to do
    await success(mongoose);
    await mongoose.connection.close();
    mongoose.modelNames().forEach((model) => {
        mongoose.deleteModel(model);
    })
    mongoose.disconnect()
}
exports.connect = connect