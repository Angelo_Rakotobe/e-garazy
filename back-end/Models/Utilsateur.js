const utils = require('../Tools/utils');
const IsEmail = require('isemail');
const bcrypt = require("bcrypt"); // Crypter mot de passe
const { request } = require('express');

class Utilisateur {
    mongo = {
        collectionName: "utilisateurs",
        schema: {
            nom :{ type: String },
            prenom :{ type: String },
            pseudo :{ type: String },
            email: { type: String },
            motDePasse: { type: String },
            role: { type: String }
        }
    }

    constructor(nom,prenom,pseudo, email, motDePasse, role) {
        this.nom = nom;
        this.prenom = prenom;
        this.pseudo = pseudo;
        this.email = email;
        this.motDePasse = motDePasse;
        this.role = role;
    }

    async inscription(mongoose) {
        let etat = false;
        let data = undefined;  
        let errors = {};

        const schema = new mongoose.Schema(this.mongo.schema, { versionKey: false });
        const utilisateur = mongoose.model(this.mongo.collectionName, schema);
        const filtre = { email: this.email };
        
        // If Empty
        if (!this.email || !this.motDePasse) {
                etat  = false,
                errors = { '_global_': "Merci de remplir tous les champs." }
        }else{
            // Verifier Email
            const checkEmail = IsEmail.validate(this.email);
            if (checkEmail) {
                await utilisateur.find(filtre).then((doc)=>{
                    if (doc.length != 0) {errors.email = "L'Email existe deja.";}
                });
            } else {
                errors.email = "Email non valide.";
            }
            // Verifier nom Utilisateur
            await utilisateur.findOne({pseudo: new RegExp('^'+this.pseudo+'$', "i")}).then((doc) =>{
                if (doc) errors.pseudo = "Le pseudo est deja pris.";
            });
            // Verifier mot de motDePasse
            if (this.motDePasse.length < 6) errors.motDePasse = "Le mot de passe doit etre au moins de 6 caracteres."
            else var motDePasseCrypter = bcrypt.hashSync(this.motDePasse, 8);
        }
        if (Object.keys(errors).length === 0) {
            etat = true
            data = {
                nom: this.nom,
                prenom: this.prenom,
                pseudo: this.pseudo,
                email: this.email,    
                motDePasse: motDePasseCrypter,
                role: "client"
            }
            var user = new utilisateur(data);
            await user.save(); //Insert data
        }
        return { success: etat, errors: errors, data: data }
    }

    async connexion(mongoose) {
        let success = true;
        let errors = undefined;
        let data = undefined;

        const utilisateur = mongoose.model(this.mongo.collectionName, this.mongo.schema);
        const filtre = { email: this.email }
        await utilisateur.find(filtre).then((doc) => {
            if (doc.length == 0) {
                success = false;
                errors = { 'email': "Aucun compte n'est associer a ce mail" }
            }
            else {
                let utilisateurDB = doc[0];
                var verifyPassword = bcrypt.compareSync(this.motDePasse, utilisateurDB.motDePasse); //Comparer mot de passe (Boolean) 
                if (!verifyPassword) {
                    success = false;
                    errors = { '_global_': "Email ou mot de passe incorect!" }
                }
                else {
                    data = {
                        email: utilisateurDB.email,
                            // role: utilisateur.role,
                            token: {
                            value: utils.token(),
                            expiration: new Date()
                        }
                    }
                }
            }
        })
        return { success: success, errors: errors, data: data };
    }

}
module.exports = Utilisateur;