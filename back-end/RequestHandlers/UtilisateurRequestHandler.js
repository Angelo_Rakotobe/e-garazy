const Utilisateur = require('../Models/Utilsateur');
const HttpResponse = require('../Tools/HttpResponse');
const connection = require('../Tools/Connection');

// INSCRIPTION
exports.inscription = async (request, response, next) => {  
    let httpResponse = new HttpResponse();
    try{
        let utilisateur = new Utilisateur(request.body.nom,request.body.prenom,request.body.pseudo, request.body.email, request.body.motDePasse, request.body.role)
        const mongoose = await connection.connect(async (mongoose) => {
            let objConnexion = await utilisateur.inscription(mongoose);

            if(objConnexion.success){
                httpResponse.setStatus(200);
                httpResponse.setData(objConnexion.data);
                httpResponse.statusMessage = "Inscription réussie";
            }
            else{
                httpResponse.setStatus(400);
                httpResponse.setErrors(objConnexion.errors);
            }
            response.status(httpResponse.getStatus());
            response.send(httpResponse);    
        })   
    }catch (error) {
        httpResponse.setStatus(500);
        httpResponse.setErrors(error.message)
        response.status(httpResponse.getStatus());
        response.send(httpResponse);    
    }
};
// CONNEXION
exports.connexion = async (request, response, next) => {
    let httpResponse = new HttpResponse();
    try {
        let utilisateur = new Utilisateur(request.body.nom,request.body.prenom,request.body.pseudo,request.body.email, request.body.motDePasse, request.body.role)
        const mongoose = await connection.connect(async (mongoose) => {
            let objConnexion = await utilisateur.connexion(mongoose);

            if(objConnexion.success){
                httpResponse.setStatus(200);
                httpResponse.setData(objConnexion.data);
            }
            else{
                httpResponse.setStatus(400);
                httpResponse.setErrors(objConnexion.errors);
            }
            response.status(httpResponse.getStatus());
            response.send(httpResponse);    
        })   
    } catch (error) {
        httpResponse.setStatus(500);
        httpResponse.setErrors(error.message)
        response.status(httpResponse.getStatus());
        response.send(httpResponse);    
    }
};