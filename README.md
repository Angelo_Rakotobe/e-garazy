# Vakio alohaaa : 
- Reefa avy mi-clone
```sh
npm install
```

- Mamorona token @ izay tsy adalainy reefa mi-push na mi-pull dia aveo executeo ity (jereo @net oe aona mcreer token de aza asina date d'expiration):
```sh
https://{{utilisateur}}:{{token}}/sombiniaina-fitahiana-randriamarosaina/projet-mean.git
```
> NB : soloina ilay `{{utilisateur}}` sy `{{token}}` ahh `!!!!!!`

- Ao am branch dev no miasa tsika (izany oe ao no mi-push sy mi-pull)
```sh
## Reefa i-pull
git pull origin dev

## Reefa i-pull
git push origin dev
```

- Alohan i-commit sy i-add jerevo tsara oe ao am branch dev ela
```sh
## hijerevana oe branch inona no misy anla (le misy etoile no branch misy anla)
git branch

## ifindra branch dev oatra 
git checkout dev
```
> NB : aza adino ny mi-pull aloha anao modif 

- Raha nanao modif ela nefa adinonla ny ni-pull aloha
```sh
git stash
git pull origin dev
git stash pop
```

- Reefa ilance anle application back-end
```sh
npm run dev
```
- Port `8888` no nataoko ao aloha reefa itest zany dia `https:\\localhost:8888`

**Bon dev lessy Ra Angelolakaa anhh**
